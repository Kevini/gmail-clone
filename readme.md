## Vue x Tailwindcss Builds

This is a [Vite.js](https://vitejs.dev/) + [Tailwindcss](https://tailwindcss.com/) project bootstrapped with [`npm init @vitejs/app my-vue-app --template vue`](https://vitejs.dev/guide/#scaffolding-your-first-vite-project).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Demo

For demo purposes i have used [Vercel](https://vercel.com) to deploy the application and you can view it here [Gmail Clone](https://gmail-clone-xi.vercel.app/) thanks 😎.

## Screen Shot

![Screenshot](src/assets/screen-shot.png)

## Contact

Feel free to reach out to me on [Kevin Koech](mailto:kip.kevin.kk@gmail.com?subject=[BitBucket] Source Han Sans) for any other questions or comments on this project.
